﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using MySql.Data.MySqlClient;
using NationalInstruments.TestStand.Interop.API;

using ProductDatabaseDLL;


namespace TSProductInfoFunctionsDLL
{
    public struct StationData
    {
        public string StationName;
        public string FactoryCode;
        public string StationDescription;
    }

    public struct ProductData
    {
        public string PcbaPartNumber;
        public string PcbaProductCode;
        public string HlaPartNumber;
        public string HlaProductCode;
        public string ProductDescription;
        public string Model;
        public string MacPool;
        public int NumberOfMacs;
        public int HwCode;
        public string BenchMarkPfsCode;
        public string CmCodes;
        public string FirmwareVersion;
        public string FirmwareID;
    }

    public sealed class TSProductInfoFunctions
    {
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;

        private string DBserver;
        private string DBdatabase;
        private string DBuser;
        private string DBpassword;

        // error code range:5150	5199	-8150	-8174 
        public enum SysErrorCodes
        {
            none,                       // 0 - no error
            open = -8150,               // error opening database
            no_test_found = -8151,      // no test was found for the station/factory code combo
            database_error = -8152      // problem reading database
        }


        /// <summary>
        /// Setup the function.
        /// </summary>
        /// <param name="databasename"></param>
        /// <param name="databaseServer"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public TSProductInfoFunctions(string databasename, string databaseServer, string user, string password)
        {
            DBdatabase = databasename;
            DBserver = databaseServer;
            DBuser = user;
            DBpassword = password;
        }

        public bool IsDatabasePresent(out bool errorOccurred, out int errorCode, out string errorMsg)
        { 
            ProductDatabase prodDB = new ProductDatabase();

            if (!prodDB.Open(DBserver, DBdatabase, DBuser, DBpassword))    // if there was a error opening the database
            {
                errorCode = (int)SysErrorCodes.open;
                errorMsg = prodDB.LastErrorMessage;
                errorOccurred = true;
                return false;
            }

            // database opened
            prodDB.Close();            // close it. just seeing if it can be opened.
            errorOccurred = false;
            errorCode = 0;
            errorMsg = string.Empty;
            return true;
        }

        /// <summary>
        /// Will display a operator screen to select the product to test from a list of valid products for this station
        /// </summary>
        /// <param name="stationName"></param>
        /// <param name="factoryCode"></param>
        /// <param name="errorOccurred"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public ProductData SelectProductForStation(string stationName, string factoryCode, out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            ProductData[] productList;
            ProductData selectedProduct = new ProductData();
            string pickedItem;

            productList = GetAllProductsForStation(stationName, factoryCode, out errorOccurred, out errorCode, out errorMsg);
            if (errorOccurred)
            {
                return selectedProduct;
            }
            
            SelectProduct iform = new SelectProduct(productList);
            iform.ShowDialog();
            pickedItem = iform.PickedBenchMarkHlaPn;
            iform.Dispose();
            for (int i = 0; i < productList.Length; i++)
            {
                if (productList[i].HlaPartNumber == pickedItem)
                    selectedProduct = productList[i];
            }

            errorCode = 0;
            errorMsg = string.Empty;
            errorOccurred = false;
            return selectedProduct;

        }

        /// <summary>
        /// Will get the data for a HLA PN for this station
        /// </summary>
        /// <param name="stationName"></param>
        /// <param name="factoryCode"></param>
        /// <param name="errorOccurred"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public ProductData GetHLAProductForStation(string stationName, string factoryCode, string hlaPN, out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            ProductData[] productList;
            ProductData selectedProduct = new ProductData();

            productList = GetAllProductsForStation(stationName, factoryCode, out errorOccurred, out errorCode, out errorMsg);
            if (errorOccurred)
            {
                return selectedProduct;
            }

            
            for (int i = 0; i < productList.Length; i++)
            {
                if (productList[i].HlaPartNumber == hlaPN)
                    selectedProduct = productList[i];
            }

            if (selectedProduct.HlaPartNumber == string.Empty)
            {
                errorCode = (int)SysErrorCodes.no_test_found;
                errorMsg = "No data found for " + hlaPN;
                errorOccurred = true;
                return selectedProduct;
            }
            errorCode = 0;
            errorMsg = string.Empty;
            errorOccurred = false;
            return selectedProduct;

        }



        /// <summary>
        /// Top function which will get the list of products that can be tested on this station
        /// </summary>
        /// <param name="factorycode"></param>
        /// <param name="stationName"></param>
        /// <param name="errorOccurred"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMsg"></param>
        public ProductData[] GetAllProductsForStation(string stationName, string factoryCode, out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            ProductDatabase prodDB = new ProductDatabase();

            List<ProductDatabase.Product_data> fullList = new List<ProductDatabase.Product_data>();
            List<ProductData> editedList = new List<ProductData>();
            errorOccurred = false;
            errorCode = 0;
            errorMsg = string.Empty;

            if (!prodDB.Open(DBserver, DBdatabase, DBuser, DBpassword))    // if there was a error opening the database
            {
                errorCode = (int)SysErrorCodes.open;
                errorMsg = prodDB.LastErrorMessage;
                errorOccurred = true;
                return editedList.ToArray();
            }

            if (!prodDB.GetStationProductData(stationName, out fullList, out errorMsg))
            {
                errorCode = prodDB.LastErrorCode;
                errorOccurred = true;
            }
            prodDB.Close();

           foreach ( var item in fullList)
            {
                if (item.active_part & (item.cm_codes.Contains(factoryCode)))   // is active part and made in this factory
                {
                    ProductData single = new ProductData();
                    single.PcbaPartNumber = item.pcba_used;
                    single.PcbaProductCode = item.pcba_product_code;
                    single.HlaPartNumber = item.pn;
                    single.HlaProductCode = item.product_code;
                    single.ProductDescription = item.product_desc;
                    single.Model = item.model;
                    single.MacPool = item.family_code;
                    single.BenchMarkPfsCode = item.pfs_assembly_number;
                    single.NumberOfMacs = item.number_of_macs;
                    single.HwCode = item.hw_code;
                    single.FirmwareVersion = item.firmware_version;
                    single.FirmwareID = item.firmware_id;
                    editedList.Add(single);
                }
            }

            return editedList.ToArray();
        }



        /*
                //--------------------------------------
                // internal functions
                //

                private string GetValidTestID (string factorycode, string productcode, string version,
                                                out bool errorOccurred, out int errorCode, out string errorMsg)
                {
                    string result = string.Empty;
                    errorCode = 0;                      // set to no errors
                    errorOccurred = false;
                    errorMsg = string.Empty;


                    try
                    {
                        connection.Open();
                    }
                    catch (Exception ex)
                    {
                        errorMsg = "Error opening " + databaseInfo.database + " on server " + databaseInfo.server + ". " + ex.Message;
                        errorCode = (int)SysErrorCodes.open;
                        errorOccurred = true;               // system error
                        return result;
                    }

                    cmd = connection.CreateCommand();

                    cmd.CommandText = "SELECT testing.testprogram_id"
                                        + " FROM(product_info.code_version code_version"
                                                + " INNER JOIN product_info.product_codes product_codes"
                                                   + " ON(code_version.product_codes_id = product_codes.product_code))"
                                               + " INNER JOIN product_info.testing testing"
                                                  + " ON(testing.code_version_id = code_version.id)"
                                         + " WHERE(product_codes.product_code = '" + productcode + "')"
                                               + " AND(code_version.version = '" + version + "')"
                                               + " AND(testing.allowed_factories LIKE '" + factorycode + "')"
                                               + " AND(code_version.active) AND(testing.active);";
                    try
                    {
                        rdr = cmd.ExecuteReader();
                        if (rdr.HasRows)        // if there is data
                        {
                            while(rdr.Read())
                            {
                                if (result == string.Empty)     // if this is the first active seq name
                                    result = rdr.GetString(0);
                                else
                                {
                                    result += "," + rdr.GetString(0);
                                }
                            }
                        }
                        else
                        {
                            errorMsg = "No data found for product code: " + productcode + ", version:" + version + ", factory code:" + factorycode;
                            errorCode = (int)SysErrorCodes.no_test_found;
                            errorOccurred = true;               // system error
                        }
                    }
                    catch (Exception ex)
                    {
                        errorMsg = databaseInfo.database + " database error:" + ex.Message;
                        errorCode = (int)SysErrorCodes.database_error;
                        errorOccurred = true;               // system error
                    }
                    connection.Close();
                    return result;
                }
                */
    }
}
